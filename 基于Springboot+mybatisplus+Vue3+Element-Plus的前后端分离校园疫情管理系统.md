# 基于Springboot+mybatisplus+Vue3+Element-Plus的前后端分离校园疫情管理系统

【作者:  **Coding路人王：从0到1**】

## 一、技术选型

**Java:**  JDK1.8

**前端技术：**Vue3、vite3、ES6、Element-Plus、TypeScript、Axios

**后端技术**：Springboot2、Mybatisplus、Shiro、HttpClient爬虫

**数据库：**Mysql 5或8

**缓存：**Redis

**数据源：**腾讯、网易云

**开发工具：**VScode、IDEA、Maven、Navicat、Git【可有可无】

**安装好所需环境，都比较简单，忽略环境的安装**

- [Node.js](http://nodejs.org/) 版本要求`14.x`以上，这里推荐 `16.x` 及以上。

- ```xml
  # 选择性：全局安装 pnpm
  npm i -g pnpm
  # 验证
  pnpm -v
  ```

### 1.1 前端选型

vue-element-plus-admin 前端快速框架

**下载地址：**https://gitee.com/wang_yining/vue-element-plus-admin

**操作文档：**https://element-plus-admin-doc.cn/guide/introduction.html

**简介**

[vue-element-plus-admin](https://github.com/kailong321200875/vue-element-plus-admin) 是一个基于 [element-plus](https://element-plus.org/) 免费开源的中后台模版。使用了最新的 [Vue3](https://github.com/vuejs/vue-next)，[Vite3](https://github.com/vitejs/vite)，[Typescript](https://www.typescriptlang.org/)等主流技术开发，开箱即用的中后台前端解决方案，可以用来作为项目的启动模版，也可用于学习参考。并且时刻关注着最新技术动向，尽可能的第一时间更新。

[vue-element-plus-admin](https://github.com/kailong321200875/vue-element-plus-admin) 的定位是后台集成方案，因为集成了很多你可能用不到的功能，会造成不少的代码冗余。如果你的项目不关注这方面的问题，也可以直接基于它进行二次开发。

注意

- 由于精力有限，[template](https://github.com/kailong321200875/vue-element-plus-admin/tree/template) 分支将不再维护，如果需要精简版，请自行删除不需要的文件及代码。

**需要掌握的基础知识**

本项目需要一定前端基础知识，请确保掌握 Vue 的基础知识，以便能处理一些常见的问题。

为了能快速上手本项目，请先大致浏览一遍文档及在线示例。

建议在开发前先学一下以下内容，提前了解和学习这些知识，会对项目理解非常有帮助:

- [Vue3](https://v3.vuejs.org/)
- [Pinia](https://pinia.vuejs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Vue-router](https://next.router.vuejs.org/)
- [Element-plus](https://element-plus.org/)
- [Es6](https://es6.ruanyifeng.com/)
- [Vitejs](https://vitejs.dev/)
- [WindiCss](https://windicss.netlify.app/)
- [Axios](https://axios-http.com/)

**目录结构**

```xml
.
├── .github # github workflows 相关
├── .husky # husky 配置
├── .vscode # vscode 配置
├── mock # 自定义 mock 数据及配置
├── public # 静态资源
├── src # 项目代码
│   ├── api # api接口管理
│   ├── assets # 静态资源
│   ├── components # 公用组件
│   ├── hooks # 常用hooks
│   ├── layout # 布局组件
│   ├── locales # 语言文件
│   ├── plugins # 外部插件
│   ├── router # 路由配置
│   ├── store # 状态管理
│   ├── styles # 全局样式
│   ├── utils # 全局工具类
│   ├── views # 路由页面
│   ├── App.vue # 入口vue文件
│   ├── main.ts # 主入口文件
│   └── permission.ts # 路由拦截
├── types # 全局类型
├── .env.base # 本地开发环境 环境变量配置
├── .env.dev # 打包到开发环境 环境变量配置
├── .env.gitee # 针对 gitee 的环境变量 可忽略
├── .env.pro # 打包到生产环境 环境变量配置
├── .env.test # 打包到测试环境 环境变量配置
├── .eslintignore # eslint 跳过检测配置
├── .eslintrc.js # eslint 配置
├── .gitignore # git 跳过配置
├── .prettierignore # prettier 跳过检测配置
├── .stylelintignore # stylelint 跳过检测配置
├── .versionrc 自动生成版本号及更新记录配置
├── CHANGELOG.md # 更新记录
├── commitlint.config.js # git commit 提交规范配置
├── index.html # 入口页面
├── package.json
├── .postcssrc.js # postcss 配置
├── prettier.config.js # prettier 配置
├── README.md # 英文 README
├── README.zh-CN.md # 中文 README
├── stylelint.config.js # stylelint 配置
├── tsconfig.json # typescript 配置
├── vite.config.ts # vite 配置
└── windi.config.ts # windicss 配置
```

#### 1.1.1 安装 前端框架

```
1. 克隆【没有Git  直接下载压缩包，使用vscode打开】
git clone https://gitee.com/wang_yining/vue-element-plus-admin.git
2. 安装依赖
cd vue-element-plus-admin
npm install
3. 运行
npm run dev
```



#### 1.1.2  

### 1.2 后端选型

#### 1.2.1 构建springboot项目

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.5.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
  </parent>	



<!--面向切面-->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-jdbc</artifactId>
    </dependency>
    
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!--面向切面-->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-aop</artifactId>
    </dependency>

    <!--druid-->
    <dependency>
      <groupId>com.alibaba</groupId>
      <artifactId>druid-spring-boot-starter</artifactId>
      <version>1.1.20</version>
    </dependency>

    <!--log4j-->
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
    </dependency>

    <!--热部署-->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <scope>runtime</scope>
      <optional>true</optional>
    </dependency>

    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.40</version>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-configuration-processor</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <optional>true</optional>
    </dependency>

    <!--mybatisplus-->
    <dependency>
      <groupId>com.baomidou</groupId>
      <artifactId>mybatis-plus-boot-starter</artifactId>
      <version>3.1.1</version>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
```

```yml
server:
  port: 8000
#配置数据源
spring:
  datasource:
    druid:
      driver-class-name: com.mysql.jdbc.Driver
      url: jdbc:mysql://localhost:3306/covid?useUnicode=true&characterEncoding=utf8&useSSL=true&serverTimezone=UTC&useSSL=false
      username: root
      password: 123456
      max-active: 20
      max-wait: 5000
      initial-size: 1
      filters: stat,log4j,wall
      validation-query: SELECT 'X'   #验证连接
      enable: true
      #监控配置
      stat-view-servlet:
        enabled: true
        login-username: root
        login-password: 123456
        url-pattern: /druid/*

  servlet:
    multipart:
      max-file-size: 1MB
      max-request-size: 10MB

#配置mybatisplus
mybatis-plus:
  mapper-locations: classpath:mapper/*/*Mapper.xml
  global-config:
    db-config:
      id-type: auto
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl


```

#### 1.2.2 搭建mybatisplus框架

